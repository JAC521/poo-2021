package pregunta2;

public interface Evaluable {
    public void cambiarNotaEvaluacion(Double notaEvaluacion) throws ExcepcionNegativo;
    public Double obtenerNotaEvaluacion();
}

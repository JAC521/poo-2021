package pregunta2;

import Utilidades.Separador;
import java.util.HashMap;
import java.util.Map;

public class ListaPostulantes {
    private Map<String, Postulante> listaPostulantes = new HashMap<String, Postulante>();

    public void añadirPostulante(Postulante postulante){
        try{
            listaPostulantes.put(postulante.getDni(), postulante);
        }catch(ExcepcionNegativo e){
            System.out.println(e.getMessage());
        }
    }
}

package pregunta2;

public class Postulante implements Evaluable{
    private String dni;
    private String nombres;
    private String apellidos;
    private NivelEducativo nivelEducativo;
    private String direccion;
    private String numeroTelefonico;
    private Integer edad;
    private Double notaEvaluacion;

    public Postulante(String dni, String nombres, String apellidos, NivelEducativo nivelEducativo,
    String direccion, String numeroTelefonico, Integer edad, Double notaEvaluacion){
        this.dni = dni;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.nivelEducativo = nivelEducativo;
        this.direccion = direccion;
        this.numeroTelefonico = numeroTelefonico;
        this.edad = edad;
    }

    public void cambiarNotaEvaluacion(Double notaEvaluacion) throws ExcepcionNegativo{
        if(notaEvaluacion < 0){
            throw new ExcepcionNegativo();
        }else{
            this.notaEvaluacion = notaEvaluacion;
        }
    }

    public Double obtenerNotaEvaluacion(){
        return this.notaEvaluacion;
    }

    public String getDni(){
        return this.dni;
    }
}

package pregunta3;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import Utilidades.Separador;

public class ListaVentas {
    private Map<Reloj, Integer> ventasRelojes = new HashMap<Reloj, Integer>();
    private List<Reloj> listaProductosVendidos = new ArrayList<Reloj>();
    private Reloj[] rankingProductos;

    public void nuevaVenta(Reloj productoVendido, Integer cantidad){
        if(ventasRelojes.containsKey(productoVendido)){
            ventasRelojes.replace(productoVendido, ventasRelojes.get(productoVendido)+cantidad);
        }else{
            ventasRelojes.put(productoVendido, cantidad);
            listaProductosVendidos.add(productoVendido);
        }

    }

    private void cambiarPosicionRanking(int posicion1, int posicion2){
        Reloj aux = (Reloj)rankingProductos[posicion1];
        rankingProductos[posicion1] = (Reloj)rankingProductos[posicion2];
        rankingProductos[posicion2] = (Reloj)aux;
    }

    private void ordenarRanking(){
        rankingProductos = new Reloj[listaProductosVendidos.size()];
        rankingProductos = (Reloj[])listaProductosVendidos.toArray();
        for(int i = 0; i < (listaProductosVendidos.size()-1); i++){
            for(int j = i+1; j < listaProductosVendidos.size(); j++){
                if(getMontoProducto(listaProductosVendidos.get(i)) < getMontoProducto(listaProductosVendidos.get(j))){
                    cambiarPosicionRanking(i, j);
                }
            }
        }
    }

    public Double getMontoProducto(Reloj producto){
        return ventasRelojes.get(producto)*producto.getPrecio();
    }

    public void mostrarVentasProducto(Reloj producto){
        producto.mostrarDatos();
        System.out.println("Cantidad vendida: " + ventasRelojes.get(producto));
        System.out.println("Monto total: S/. " + getMontoProducto(producto));
    }

    public void mostrarPuestoRanking(Integer posicion){
        rankingProductos[posicion].mostrarDatos();
        System.out.println("Cantidad vendida: " + ventasRelojes.get(rankingProductos[posicion]));
        System.out.println("Monto total: S/. " + ventasRelojes.get(rankingProductos[posicion])*rankingProductos[posicion].getPrecio());
    }

    public void mostrarVentas(){
        System.out.println(Separador.separador());
        System.out.println("Lista de ventas");
        for (Reloj producto : listaProductosVendidos) {
            mostrarVentasProducto(producto);
        }
    }

    public void mostrarTop3(){
        ordenarRanking();
        System.out.println(Separador.separador());
        System.out.println("Top 3 de ventas");
        mostrarPuestoRanking(1);
        mostrarPuestoRanking(2);
        mostrarPuestoRanking(3);
    }
}

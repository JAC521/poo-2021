package pregunta3;

public class Pregunta3 {
    public static void main(String[] args) throws Exception {
        Reloj reloj1 = new Reloj(105.00, "Rolex", "Reloj Rolex simple");
        Reloj reloj2 = new Reloj(151.00, "Rolex", "Reloj Rolex de oro");
        Reloj reloj3 = new Reloj(208.00, "Rolex", "Reloj Rolex de platino");
        Reloj reloj4 = new Reloj(133.00, "Rolex", "Reloj Rolex de plata");

        ListaVentas listaVentas = new ListaVentas();

        listaVentas.nuevaVenta(reloj1, 2);
        listaVentas.nuevaVenta(reloj2, 1);
        listaVentas.nuevaVenta(reloj3, 1);
        listaVentas.nuevaVenta(reloj4, 1);
        listaVentas.nuevaVenta(reloj1, 1);
        listaVentas.nuevaVenta(reloj2, 3);

        listaVentas.mostrarVentas();
        listaVentas.mostrarTop3();
    }
}

package pregunta3;

import Utilidades.Separador;

public class Reloj {
    private Double precio;
    private String marca;
    private String descripcion;

    public Reloj(Double precio, String marca, String descripcion){
        this.precio = precio;
        this.marca = marca;
        this.descripcion = descripcion;
    }

    public void mostrarDatos(){
        System.out.println(Separador.separador());
        System.out.println("Precio unitario: S/. " + precio);
        System.out.println("Marca: " + marca);
        System.out.println("Descripcion: " + descripcion);
    }
    
    public Double getPrecio(){
        return this.precio;
    }
    public String getMarca(){
        return this.marca;
    }
    public String getDescripcion(){
        return this.descripcion;
    }

    public void setPrecio(Double precio){
        this.precio = precio;
    }
    public void setMarca(String marca){
        this.marca = marca;
    }
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }
}

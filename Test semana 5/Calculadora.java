public class Calculadora implements Calculable{
    public double sumar(double a, double b){
        return a+b;
    }
    public double restar(double a, double b){
        return a-b;
    }
    public double multiplicar(double a, double b){
        return a*b;
    }
    public double dividir(double a, double b){
        if(b != 0){
            return a/b;
        }
        else{
            System.out.println("Error");
            return 0;
        }
    }
    public double potenciar(double a, double b){
        return Math.pow(a, b);
    }
    public double sacarRaizCuadrada(double a){
        return Math.sqrt(a);
    }
}
}

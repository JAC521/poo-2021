import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<String> competidores = new ArrayList<String>();
        Map<Integer, String> puestos = new HashMap<Integer, String>();
        competidores.add("Susana");
        competidores.add("Pablo");
        competidores.add("Luis");

        puestos.put(1, competidores.get(2));
        puestos.put(2, competidores.get(1));
        puestos.put(3, competidores.get(3));
    }
}

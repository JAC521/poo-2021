public class Ordenar{
	static void intercambiar(String[] cadena, int pos1, int pos2){
		String aux = cadena[pos1];
		cadena[pos1] = cadena[pos2];
		cadena[pos2] = aux;
	}

	static void ordenarCadena(String[] cadena){
		for(int i = 0; i < cadena.length; i++){
			for(int j = i + 1; j < cadena.length; j++){
				int n = 0;
				while(n < cadena[i].length() && n < cadena[j].length()){
					if(cadena[i].charAt(n) > cadena[j].charAt(n)){
						intercambiar(cadena, i, j);
						break;
					}
					if(cadena[i].charAt(n) < cadena[j].charAt(n)){
						break;
					}
					n++;
				}
			}
		}
	}

	static void imprimirArreglo(String[] cadena){
		System.out.println("-----------------------------------");
		for(int i = 0; i < cadena.length; i++){
			System.out.println((i+1) + ". " + cadena[i]);
		}
	}

	public static void main(String[] args){
		String[] nombres = new String[]{"Rodrigez", "Cardenas", "Aguilar", "Aguirre"};

		imprimirArreglo(nombres);
		ordenarCadena(nombres);
		imprimirArreglo(nombres);
	}
}
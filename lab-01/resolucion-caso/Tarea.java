class VentasDia{
	public Venta[] ventas;

	public float calcularVentaTotal(){
		float total = 0.0f;
		for(int i = 0; i < ventas.length; i++){
			total = total + ventas[i].calcularMonto();
		}
		return total;
	}
	public float calcularVentaMayor(){
		float mayor = 0.0f;
		for(int i = 0; i < ventas.length; i++){
			if(mayor < ventas[i].calcularMonto()){
				mayor = ventas[i].calcularMonto();
			}
		}
		return mayor;
	}
	public float calcularVentaMenor(){
		float menor = ventas[0].calcularMonto();
		for(int i = 1; i < ventas.length; i++){
			if(menor > ventas[i].calcularMonto()){
				menor = ventas[i].calcularMonto();
			}
		}
		return menor;
	}
}

class Venta{
	public Producto[] productos;

	public float calcularMonto(){
		float monto = 0.0f;
		for(int i = 0; i < productos.length; i++){
			monto = monto + productos[i].montoUnidad;
		}
		return monto;
	}
}

class Producto{
	public String nombre;
	public int codigo;
	public float montoUnidad;

	public Producto(String nombre, int codigo, float montoUnidad){
		this.nombre = nombre;
		this.codigo = codigo;
		this.montoUnidad = montoUnidad;
	}
}

public class Tarea{
	public static void main(String[] args){
		Producto producto1 = new Producto("cuaderno", 101, 5.0f);
		Producto producto2 = new Producto("plumon", 105, 1.5f);
		Producto producto3 = new Producto("lapicero", 112, 0.5f);
		Producto producto4 = new Producto("tijeras", 126, 3.0f);

		Venta venta1 = new Venta();
		Venta venta2 = new Venta();
		Venta venta3 = new Venta();

		venta1.productos = new Producto[]{producto1, producto3};
		venta2.productos = new Producto[]{producto1, producto3, producto4};
		venta3.productos = new Producto[]{producto2, producto3};

		VentasDia ventasDia1 = new VentasDia();
		ventasDia1.ventas = new Venta[]{venta1, venta2, venta3};
		
		System.out.println("Venta total: " + ventasDia1.calcularVentaTotal());
		System.out.println("Venta mayor: " + ventasDia1.calcularVentaMayor());
		System.out.println("Venta menor: " + ventasDia1.calcularVentaMenor());
	}
}
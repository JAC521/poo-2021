package Test2;

public class Alarma {
    private int horas;
    private int minutos;
    private int segundos;
    private boolean activa;

    public void setHoras(){};
    public void setMinutos(){};
    public void setSegundos(){};

    public int getHoras(){};
    public int getMinutos(){};
    public int getSegundos(){};

    public void activar(){};
    public void desactivar(){};

    public boolean estaActiva();
}

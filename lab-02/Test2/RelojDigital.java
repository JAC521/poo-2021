package Test2;

public class RelojDigital {
    private int horas;
    private int minutos;
    private int segundos;
    private int dia;
    private int mes;
    private String diaSemana;
    private Alarma alarma;
    private int porcentajeBateria;

    public void setHoras(){};
    public void setMinutos(){};
    public void setSegundos(){};
    public void setDia(){};
    public void setMes(){};
    public void setDiaSemana(){};

    public int getHoras(){};
    public int getMinutos(){};
    public int getSegundos(){};
    public int getDia(){};
    public int getMes(){};
    public String getDiaSemana(){};
    public int getPorcentajeBateria(){};

    public void activarAlarma(){};
    public void desactivarAlarma(){};

    public boolean estaActivaAlarma();
}

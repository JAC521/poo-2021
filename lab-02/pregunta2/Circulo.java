package pregunta2;

import java.util.Scanner;

public class Circulo {
    private Double radio;

    public Double calcularArea(){
        return Math.pow(radio, 2)*Math.PI;
    }

    public Double getRadio(){
        return radio;
    }

    public void setRadio(Double radio){
        this.radio = radio;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese radio: ");
        Circulo circulo = new Circulo();
        circulo.setRadio(sc.nextDouble());
        System.out.println(circulo.calcularArea());
    }
}

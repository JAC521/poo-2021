package pregunta3;

public class CifradoCesar {
    public String cifrar(String codigo){
        codigo = codigo.toUpperCase();
        char[] charArray = codigo.toCharArray();
        for(int i = 0; i < codigo.length(); i++){
            int letra = charArray[i];
            if((charArray[i] + 3) > 'Z'){
                letra = letra - 23;
            }else{
                letra = letra + 3;
            }
            charArray[i] = (char)letra;
        }
        codigo = String.copyValueOf(charArray);
        return codigo;
    }

    public String descifrar(String codigo){
        codigo = codigo.toUpperCase();
        char[] charArray = codigo.toCharArray();
        for(int i = 0; i < codigo.length(); i++){
            int letra = charArray[i];
            if((charArray[i] - 3) < 'A'){
                letra = letra + 23;
            }else{
                letra = letra - 3;
            }
            charArray[i] = (char)letra;
        }
        codigo = String.copyValueOf(charArray);
        return codigo;
    }

    public static void main(String[] args){
        String codigo = "aBcDeFghijklmnopqrstuvwxyz";
        CifradoCesar a = new CifradoCesar();
        codigo = a.cifrar(codigo);
        System.out.println(codigo);
        codigo = a.descifrar(codigo);
        System.out.println(codigo);
    }
}

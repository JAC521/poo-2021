public class Calzado {
    private double precio;
    private String marca;
    private String modelo;
    private String tipo;
    private String genero;
    private String horma;
    private String material;
    private String temporada;
    private String paisOrigen;

    public Calzado(double precio, String marca, String modelo, String tipo, String genero, String horma, 
    String material, String temporada, String paisOrigen){
        this.precio = precio;
        this.marca = marca;
        this.modelo = modelo;
        this.tipo = tipo;
        this.genero = genero;
        this.horma = horma;
        this.material = material;
        this.temporada = temporada;
        this.paisOrigen = paisOrigen;
    }

    public void imprimirDatos(){
        System.out.println("---------------------------------------");
        System.out.println("Precio: S/. " + this.precio);
        System.out.println("Marca: " + this.marca);
        System.out.println("Modelo: " + this.modelo);
        System.out.println("Tipo: " + this.tipo);
        System.out.println("Genero: " + this.genero);
        System.out.println("Horma: " + this.horma);
        System.out.println("Material: " + this.material);
        System.out.println("Temporada: " + this.temporada);
        System.out.println("Pais de origen: " + this.paisOrigen);
    }

    public void setPrecio(double precio){
        this.precio = precio;
    }
    public void setMarca(String marca){
        this.marca = marca;
    }
    public void setModelo(String modelo){
        this.modelo = modelo;
    }
    public void setTipo(String tipo){
        this.tipo = tipo;
    }
    public void setGenero(String genero){
        this.genero = genero;
    }
    public void setHorma(String horma){
        this.horma = horma;
    }
    public void setMaterial(String material){
        this.material = material;
    }
    public void setTemporada(String temporada){
        this.temporada = temporada;
    }
    public void setPaisOrigen(String paisOrigen){
        this.paisOrigen = paisOrigen;
    }

    public double getPrecio(){
        return this.precio;
    }
    public String getMarca(){
        return this.marca;
    }
    public String getModelo(){
        return this.modelo;
    }
    public String getTipo(){
        return this.tipo;
    }
    public String getGenero(){
        return this.genero;
    }
    public String getHorma(){
        return this.horma;
    }
    public String getMaterial(){
        return this.material;
    }
    public String getTemporada(){
        return this.temporada;
    }
    public String getPaisOrigen(){
        return this.paisOrigen;
    }
}
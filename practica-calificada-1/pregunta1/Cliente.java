public class Cliente {
    private String nombre;
    private String dni;

    public Cliente(String nombre, String dni){
        this.nombre = nombre;
        this.dni = dni;
    }

    public void imprimirDatos(){
        System.out.println("---------------------------------------");
        System.out.println("Nombre: " + this.nombre);
        System.out.println("DNI: " + this.dni);
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public void setDni(String dni){
        this.dni = dni;
    }

    public String getNombre(){
        return this.nombre;
    }
    public String getDni(){
        return this.dni;
    }
}

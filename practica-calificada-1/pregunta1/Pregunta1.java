public class Pregunta1 {
    public static void main(String[] args){
        Calzado calzado1 = new Calzado(200, "Gucci", "Modelo 1", "Infantil", "Para mujeres", "Horma 1", "Cuero", "Primavera", "Italia");
        Calzado calzado2 = new Calzado(320, "Adidas", "Modelo 5", "Juvenil", "Para hombres", "Horma 2", "Cuero", "Verano", "Alemania");
        
        Cliente cliente1 = new Cliente("Ana Lizando Larosa", "74529754");
        Cliente cliente2 = new Cliente("Adrian Lopez Ramirez", "83979270");

        Venta venta1 = new Venta(cliente1, calzado1);
        Venta venta2 = new Venta(cliente2, calzado2);

        Venta[] ventas = new Venta[]{venta1, venta2};
        mostrarVentas(ventas);
    }

    public static void mostrarVentas(Venta[] ventas){
        for (Venta venta : ventas) {
            venta.imprimirDatos();
        }
    }
}
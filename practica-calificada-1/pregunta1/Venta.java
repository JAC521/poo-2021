public class Venta {
    private Cliente cliente;
    private Calzado producto;

    public Venta(Cliente cliente, Calzado producto){
        this.cliente = cliente;
        this.producto = producto;
    }

    public void setCliente(Cliente cliente){
        this.cliente = cliente;
    }
    public void setProducto(Calzado producto){
        this.producto = producto;
    }

    public Cliente getCliente(){
        return this.cliente;
    }
    public Calzado getProducto(){
        return this.producto;
    }

    public void imprimirDatos(){
        System.out.println("---------------------------------------");
        System.out.println("CLIENTE");
        this.cliente.imprimirDatos();
        System.out.println("---------------------------------------");
        System.out.println("PRODUCTO");
        this.producto.imprimirDatos();
    }
}

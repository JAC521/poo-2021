import java.lang.Math;

public abstract class Calcular {
    public static double area(Figura figura){
        double area = 0;
        area = figura.getCantidadDeLados()*Math.pow(figura.getLongitudLado(), 2)/(4*Math.tan(Math.PI/figura.getCantidadDeLados()));
        return area;
    }

    public static double perimetro(Figura figura){
        return figura.getCantidadDeLados()*figura.getLongitudLado();
    }

    public static int cantidadDeDiagonales(Figura figura){
        int diagonales = 0;
        diagonales = figura.getCantidadDeLados()*(figura.getCantidadDeLados()-3)/2;
        return diagonales;
    }
}

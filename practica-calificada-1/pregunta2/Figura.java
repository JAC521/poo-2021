public class Figura {
    private int cantidadDeLados;
    private double longitudLado;

    public Figura(int n, double l){
        if(n > 2 && n < 7){
            this.cantidadDeLados = n;
        }else{
            System.out.println("Cantidad de lados invalida");
        }
        if(l > 0){
            this.longitudLado = l;
        }else{
            System.out.println("Longitud de lado invalida");
        }
    }

    public void mostrarDatos(){
        System.out.println("# Lados: " + this.cantidadDeLados);
        System.out.println("Longitud de los lados: " + this.longitudLado);
    }

    public void setCantidadDeLados(int n){
        if(n > 2 && n < 7){
            this.cantidadDeLados = n;
        }else{
            System.out.println("Cantidad de lados invalida");
        }
    }
    public void setLongitudLado(double l){
        if(l > 0){
            this.longitudLado = l;
        }else{
            System.out.println("Longitud de lado invalida");
        }
    }

    public int getCantidadDeLados(){
        return this.cantidadDeLados;
    }
    public double getLongitudLado(){
        return this.longitudLado;
    }
}

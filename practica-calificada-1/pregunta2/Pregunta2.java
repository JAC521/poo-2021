public class Pregunta2 {
    public static void main(String[] args){
        Figura triangulo = new Figura(3, 5);
        Figura cuadrado = new Figura(4, 8);
        Figura pentagono = new Figura(5, 4);
        Figura hexagono = new Figura(6, 6);

        System.out.println("---------------------------------------");
        triangulo.mostrarDatos();
        System.out.println("Area: " + Calcular.area(triangulo));
        System.out.println("Perimetro: " + Calcular.perimetro(triangulo));
        System.out.println("# Diagonales: " + Calcular.cantidadDeDiagonales(triangulo));
        System.out.println("---------------------------------------");
        cuadrado.mostrarDatos();
        System.out.println("Area: " + Calcular.area(cuadrado));
        System.out.println("Perimetro: " + Calcular.perimetro(cuadrado));
        System.out.println("# Diagonales: " + Calcular.cantidadDeDiagonales(cuadrado));
        System.out.println("---------------------------------------");
        pentagono.mostrarDatos();
        System.out.println("Area: " + Calcular.area(pentagono));
        System.out.println("Perimetro: " + Calcular.perimetro(pentagono));
        System.out.println("# Diagonales: " + Calcular.cantidadDeDiagonales(pentagono));
        System.out.println("---------------------------------------");
        hexagono.mostrarDatos();
        System.out.println("Area: " + Calcular.area(hexagono));
        System.out.println("Perimetro: " + Calcular.perimetro(hexagono));
        System.out.println("# Diagonales: " + Calcular.cantidadDeDiagonales(hexagono));
    }
}

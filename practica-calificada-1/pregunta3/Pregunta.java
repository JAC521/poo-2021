public class Pregunta {
    private String enunciado;
    private String[] alternativas;
    private String respuesta;

    public void setEnunciado(String enunciado){
        this.enunciado = enunciado;
    }

    public void setAlternativas(String a, String b, String c, String d, String e, String respuesta){
        this.alternativas = new String[]{a, b, c, d, e};
        this.respuesta = respuesta;
    }

    public String getEnunciado(){
        return enunciado;
    }

    public String getRespuesta(){
        return respuesta;
    }

    public void mostrarAlternativas(){
        for (int i = 0; i < 5; i++) {
            System.out.println((i+1)+": "+alternativas[i]);
        }
    }
}

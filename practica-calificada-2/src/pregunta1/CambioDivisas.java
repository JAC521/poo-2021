package pregunta1;

import java.lang.Math;

public abstract class CambioDivisas {
    public static Double monedaADolares(Double monto, Double valorEnDolares){
        Double nuevoMonto = monto*valorEnDolares;
        nuevoMonto = redondear(nuevoMonto, 2);
        return nuevoMonto;
    }

    public static Double dolaresAMoneda(Double monto, Double valorEnDolares){
        Double nuevoMonto = monto/valorEnDolares;
        nuevoMonto = redondear(nuevoMonto, 2);
        return nuevoMonto;
    }

    private static Double redondear(Double numero, int numeroDeDecimales){
        numero = numero-(numero%(1/(Math.pow(10, numeroDeDecimales))));
        return numero;
    }
}

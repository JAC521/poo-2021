package pregunta1;

import java.util.HashMap;

public class MapaDivisas {
    private HashMap<String, Double> Divisas = new HashMap<String, Double>();

    public void añadirDivisa(String nombreDivisa, Double valorEnDolares){
        Divisas.put(nombreDivisa, valorEnDolares);
    }

    public void cambiarValorDivisa(String nombreDivisa, Double nuevoValorEnDolares){
        Divisas.replace(nombreDivisa, nuevoValorEnDolares);
    }

    public Double obtenerValorDivisa(String nombreDivisa){
        return Divisas.get(nombreDivisa);
    }
}

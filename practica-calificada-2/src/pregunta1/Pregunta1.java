package pregunta1;

public class Pregunta1 {
    public static void main(String[] args) throws Exception {
        MapaDivisas m = new MapaDivisas();
        m.añadirDivisa("Soles", 0.27);
        m.añadirDivisa("Yenes", 0.0092);
        
        String nombreDivisa = "Yenes";
        Double monto = 100000d;

        System.out.println("------------------------------------------");
        System.out.println("Cambio de " + monto + " " + nombreDivisa + " a Soles");
        System.out.println("------------------------------------------");

        monto = CambioDivisas.monedaADolares(monto, m.obtenerValorDivisa(nombreDivisa));
        monto = CambioDivisas.dolaresAMoneda(monto, m.obtenerValorDivisa("Soles"));

        System.out.println("Nuevo monto: " + monto + " Soles");
    }
}

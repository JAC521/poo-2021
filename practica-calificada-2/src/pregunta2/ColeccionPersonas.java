package pregunta2;

import java.util.ArrayList;

import pregunta2.excepciones.ExcepcionMenorDe40;

public class ColeccionPersonas {
    ArrayList<Persona> personas = new ArrayList<Persona>();

    public void agregarPersonaControlado(Persona persona){
        try{
            agregarPersona(persona);
        }catch(ExcepcionMenorDe40 e){
            e.printStackTrace();
        }
    }

    public void agregarPersona(Persona persona) throws ExcepcionMenorDe40{
        if(persona.getEdad() < 40){
            throw new ExcepcionMenorDe40();
        }
        personas.add(persona);
    }

    public void mostrarRegistro(){
        System.out.println("------------------------------");
        System.out.println("Personas registradas");
        System.out.println("------------------------------");
        for(Persona persona: personas){
            persona.mostrarDatos();
        }
    }
}

package pregunta2;

public class Persona {
    private String dni;
    private String nombres;
    private String apellidos;
    private Integer edad;

    public Persona(String dni, String nombres, String apellidos, Integer edad){
        this.dni = dni;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.edad = edad;
    }

    public String getDni(){
        return dni;
    }
    public String getNombres(){
        return nombres;
    }
    public String getApellidos(){
        return apellidos;
    }
    public Integer getEdad(){
        return edad;
    }

    public void mostrarDatos(){
        System.out.println(nombres + " " + apellidos + "-" + dni + "-" + edad);
    }
}

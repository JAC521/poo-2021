package pregunta2;

public class Pregunta2 {
    public static void main(String[] args) throws Exception {
        ColeccionPersonas p = new ColeccionPersonas();

        Persona Juan = new Persona("71329858", "Juan", "Aguilar Romero", 42);
        Persona Pedro = new Persona("71324289", "Pedro", "Romero Aguilar", 24);
        Persona Alice = new Persona("29329858", "Alice", "Rodrigues Meza", 56);

        p.agregarPersonaControlado(Juan);
        p.agregarPersonaControlado(Pedro);
        p.agregarPersonaControlado(Alice);

        p.mostrarRegistro();
    }
}

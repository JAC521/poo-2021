package pregunta2.excepciones;

public class ExcepcionMenorDe40 extends Exception{
    public ExcepcionMenorDe40(){
        super("No se permiten personas menores de 40");
    }
}
